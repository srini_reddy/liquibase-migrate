# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This project uses liquibase to manage MySQL DB schema migration. You need to install Maven to run the related commands.


### How do I get set up? ###

* Database configuration

  JDBC configuration is in pom.xml

* Help:
  
```
#!shell

$ mvn liquibase:help
```


* Check change sets:
  
```
#!shell

$ mvn resources:resources liquibase:status -Plocal
$ mvn resources:resources liquibase:status -Pdev
$ mvn resources:resources liquibase:status -Pprod

```


* Deployment instructions
  
```
#!shell

$ mvn resources:resources liquibase:update -Plocal
$ mvn resources:resources liquibase:update -Pdev
$ mvn resources:resources liquibase:update -Pprod

```


### Who do I talk to? ###

* Repo owner or admin: Pete Tian pete@datacubes.com
* Other community or team contact:
  Pull request reviewers: Pete Tian, Yi Huang